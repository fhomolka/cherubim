#include <iostream>
#include <string>

#include "scripting/ScriptEngine.h"

#include "rl.h"

int main(int argc, char const *argv[])
{
	(void) argc;
	(void) argv;

	InitScriptingEngine();
	CompileScript();
	
	rl::InitWindow(800, 600, "Cherubim");
	rl::SetTargetFPS(60);

	RunScriptInit();
	while(!rl::WindowShouldClose())
	{
		RunScriptTick();
		rl::BeginDrawing();
		RunScriptDraw();
		rl::EndDrawing();
	};

	return 0;
}