# Libraries used 

## AngelScript
- Version: 2.36.1
- License: Zlib
- Acquired from: https://www.angelcode.com/angelscript/downloads.html

## raylib
- Version: 4.5
- License: Zlib
- Acquired from: https://github.com/raysan5/raylib/tree/fec96137e8d10ee6c88914fbe5e5429c13ee1dac