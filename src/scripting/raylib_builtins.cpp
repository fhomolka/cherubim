#include "angelscript.h"
#include "raylib.h"

#include <string>

Color asLIGHTGRAY = LIGHTGRAY;
Color asGRAY = GRAY;
Color asDARKGRAY = DARKGRAY;
Color asYELLOW = YELLOW;
Color asGOLD = GOLD;
Color asORANGE = ORANGE;
Color asPINK = PINK;
Color asRED = RED;
Color asMAROON = MAROON;
Color asGREEN = GREEN;
Color asLIME = LIME;
Color asDARKGREEN = DARKGREEN;
Color asSKYBLUE = SKYBLUE;
Color asBLUE = BLUE;
Color asDARKBLUE = DARKBLUE;
Color asPURPLE = PURPLE;
Color asVIOLET = VIOLET;
Color asDARKPURPLE = DARKPURPLE;
Color asBEIGE = BEIGE;
Color asBROWN = BROWN;
Color asDARKBROWN = DARKBROWN;

Color asWHITE = WHITE;
Color asBLACK = BLACK;
Color asBLANK = BLANK;
Color asMAGENTA = MAGENTA;
Color asRAYWHITE = RAYWHITE;

inline void RegisterStructs(asIScriptEngine* asEngine)
{
	int r;

	r = asEngine->RegisterObjectType("Color", sizeof(Color), asOBJ_VALUE | asOBJ_POD);
	r = asEngine->RegisterObjectProperty("Color", "uint8 r", asOFFSET(Color, r));
	r = asEngine->RegisterObjectProperty("Color", "uint8 g", asOFFSET(Color, g));
	r = asEngine->RegisterObjectProperty("Color", "uint8 b", asOFFSET(Color, b));
	r = asEngine->RegisterObjectProperty("Color", "uint8 a", asOFFSET(Color, a));

	r = asEngine->RegisterObjectType("Vector2", sizeof(Vector2), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<Vector2>());
	r = asEngine->RegisterObjectProperty("Vector2", "float x", asOFFSET(Vector2, x));
	r = asEngine->RegisterObjectProperty("Vector2", "float y", asOFFSET(Vector2, y));

	r = asEngine->RegisterObjectType("Rectangle", sizeof(Rectangle), asOBJ_VALUE | asOBJ_POD);
	r = asEngine->RegisterObjectProperty("Rectangle", "float x", asOFFSET(Rectangle, x));
	r = asEngine->RegisterObjectProperty("Rectangle", "float y", asOFFSET(Rectangle, y));
	r = asEngine->RegisterObjectProperty("Rectangle", "float width", asOFFSET(Rectangle, width));
	r = asEngine->RegisterObjectProperty("Rectangle", "float height", asOFFSET(Rectangle, height));

	r = asEngine->RegisterObjectType("Texture", sizeof(Texture), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<Texture>());
	r = asEngine->RegisterObjectProperty("Texture", "uint id", asOFFSET(Texture, id));
	r = asEngine->RegisterObjectProperty("Texture", "int width", asOFFSET(Texture, width));
	r = asEngine->RegisterObjectProperty("Texture", "int height", asOFFSET(Texture, height));
	r = asEngine->RegisterObjectProperty("Texture", "int mipmaps", asOFFSET(Texture, mipmaps));
	r = asEngine->RegisterObjectProperty("Texture", "int format", asOFFSET(Texture, format));
}

inline void RegisterEnums(asIScriptEngine* asEngine)
{
	const char *KeyboardKeyStr = "KeyboardKey";
	asEngine->RegisterEnum(KeyboardKeyStr);

#define REGISTER_KEY(x, y) asEngine->RegisterEnumValue(KeyboardKeyStr, x, y)

	REGISTER_KEY("KEY_NULL", KeyboardKey::KEY_NULL);
	REGISTER_KEY("KEY_APOSTROPHE", KeyboardKey::KEY_APOSTROPHE);
	REGISTER_KEY("KEY_COMMA", KeyboardKey::KEY_COMMA);
	REGISTER_KEY("KEY_MINUS", KeyboardKey::KEY_MINUS);
	REGISTER_KEY("KEY_PERIOD", KeyboardKey::KEY_PERIOD);
	REGISTER_KEY("KEY_SLASH", KeyboardKey::KEY_SLASH);
	REGISTER_KEY("KEY_ZERO", KeyboardKey::KEY_ZERO);
	REGISTER_KEY("KEY_ONE", KeyboardKey::KEY_ONE);
	REGISTER_KEY("KEY_TWO", KeyboardKey::KEY_TWO);
	REGISTER_KEY("KEY_THREE", KeyboardKey::KEY_THREE);
	REGISTER_KEY("KEY_FOUR", KeyboardKey::KEY_FOUR);
	REGISTER_KEY("KEY_FIVE", KeyboardKey::KEY_FIVE);
	REGISTER_KEY("KEY_SIX", KeyboardKey::KEY_SIX);
	REGISTER_KEY("KEY_SEVEN", KeyboardKey::KEY_SEVEN);
	REGISTER_KEY("KEY_EIGHT", KeyboardKey::KEY_EIGHT);
	REGISTER_KEY("KEY_NINE", KeyboardKey::KEY_NINE);
	REGISTER_KEY("KEY_SEMICOLON", KeyboardKey::KEY_SEMICOLON);
	REGISTER_KEY("KEY_EQUAL", KeyboardKey::KEY_EQUAL);
	REGISTER_KEY("KEY_A", KeyboardKey::KEY_A);
	REGISTER_KEY("KEY_B", KeyboardKey::KEY_B);
	REGISTER_KEY("KEY_C", KeyboardKey::KEY_C);
	REGISTER_KEY("KEY_D", KeyboardKey::KEY_D);
	REGISTER_KEY("KEY_E", KeyboardKey::KEY_E);
	REGISTER_KEY("KEY_F", KeyboardKey::KEY_F);
	REGISTER_KEY("KEY_G", KeyboardKey::KEY_G);
	REGISTER_KEY("KEY_H", KeyboardKey::KEY_H);
	REGISTER_KEY("KEY_I", KeyboardKey::KEY_I);
	REGISTER_KEY("KEY_J", KeyboardKey::KEY_J);
	REGISTER_KEY("KEY_K", KeyboardKey::KEY_K);
	REGISTER_KEY("KEY_L", KeyboardKey::KEY_L);
	REGISTER_KEY("KEY_M", KeyboardKey::KEY_M);
	REGISTER_KEY("KEY_N", KeyboardKey::KEY_N);
	REGISTER_KEY("KEY_O", KeyboardKey::KEY_O);
	REGISTER_KEY("KEY_P", KeyboardKey::KEY_P);
	REGISTER_KEY("KEY_Q", KeyboardKey::KEY_Q);
	REGISTER_KEY("KEY_R", KeyboardKey::KEY_R);
	REGISTER_KEY("KEY_S", KeyboardKey::KEY_S);
	REGISTER_KEY("KEY_T", KeyboardKey::KEY_T);
	REGISTER_KEY("KEY_U", KeyboardKey::KEY_U);
	REGISTER_KEY("KEY_V", KeyboardKey::KEY_V);
	REGISTER_KEY("KEY_W", KeyboardKey::KEY_W);
	REGISTER_KEY("KEY_X", KeyboardKey::KEY_X);
	REGISTER_KEY("KEY_Y", KeyboardKey::KEY_Y);
	REGISTER_KEY("KEY_Z", KeyboardKey::KEY_Z);
	REGISTER_KEY("KEY_LEFT_BRACKET", KeyboardKey::KEY_LEFT_BRACKET);
	REGISTER_KEY("KEY_BACKSLASH", KeyboardKey::KEY_BACKSLASH);
	REGISTER_KEY("KEY_RIGHT_BRACKET", KeyboardKey::KEY_RIGHT_BRACKET);
	REGISTER_KEY("KEY_GRAVE", KeyboardKey::KEY_GRAVE);
	// Function keys
	REGISTER_KEY("KEY_SPACE", KeyboardKey::KEY_SPACE);
	REGISTER_KEY("KEY_ESCAPE", KeyboardKey::KEY_ESCAPE);
	REGISTER_KEY("KEY_ENTER", KeyboardKey::KEY_ENTER);
	REGISTER_KEY("KEY_TAB", KeyboardKey::KEY_TAB);
	REGISTER_KEY("KEY_BACKSPACE", KeyboardKey::KEY_BACKSPACE);
	REGISTER_KEY("KEY_INSERT", KeyboardKey::KEY_INSERT);
	REGISTER_KEY("KEY_DELETE", KeyboardKey::KEY_DELETE);
	REGISTER_KEY("KEY_RIGHT", KeyboardKey::KEY_RIGHT);
	REGISTER_KEY("KEY_LEFT", KeyboardKey::KEY_LEFT);
	REGISTER_KEY("KEY_DOWN", KeyboardKey::KEY_DOWN);
	REGISTER_KEY("KEY_UP", KeyboardKey::KEY_UP);
	REGISTER_KEY("KEY_PAGE_UP", KeyboardKey::KEY_PAGE_UP);
	REGISTER_KEY("KEY_PAGE_DOWN", KeyboardKey::KEY_PAGE_DOWN);
	REGISTER_KEY("KEY_HOME", KeyboardKey::KEY_HOME);
	REGISTER_KEY("KEY_END", KeyboardKey::KEY_END);
	REGISTER_KEY("KEY_CAPS_LOCK", KeyboardKey::KEY_CAPS_LOCK);
	REGISTER_KEY("KEY_SCROLL_LOCK", KeyboardKey::KEY_SCROLL_LOCK);
	REGISTER_KEY("KEY_NUM_LOCK", KeyboardKey::KEY_NUM_LOCK);
	REGISTER_KEY("KEY_PRINT_SCREEN", KeyboardKey::KEY_PRINT_SCREEN);
	REGISTER_KEY("KEY_F1", KeyboardKey::KEY_F1);
	REGISTER_KEY("KEY_F2", KeyboardKey::KEY_F2);
	REGISTER_KEY("KEY_F3", KeyboardKey::KEY_F3);
	REGISTER_KEY("KEY_F4", KeyboardKey::KEY_F4);
	REGISTER_KEY("KEY_F5", KeyboardKey::KEY_F5);
	REGISTER_KEY("KEY_F6", KeyboardKey::KEY_F6);
	REGISTER_KEY("KEY_F7", KeyboardKey::KEY_F7);
	REGISTER_KEY("KEY_F8", KeyboardKey::KEY_F8);
	REGISTER_KEY("KEY_F9", KeyboardKey::KEY_F9);
	REGISTER_KEY("KEY_F10", KeyboardKey::KEY_F10);
	REGISTER_KEY("KEY_F11", KeyboardKey::KEY_F11);
	REGISTER_KEY("KEY_F12", KeyboardKey::KEY_F12);
	REGISTER_KEY("KEY_LEFT_SHIFT", KeyboardKey::KEY_LEFT_SHIFT);
	REGISTER_KEY("KEY_LEFT_CONTROL", KeyboardKey::KEY_LEFT_CONTROL);
	REGISTER_KEY("KEY_LEFT_ALT", KeyboardKey::KEY_LEFT_ALT);
	REGISTER_KEY("KEY_LEFT_SUPER", KeyboardKey::KEY_LEFT_SUPER);
	REGISTER_KEY("KEY_RIGHT_SHIFT", KeyboardKey::KEY_RIGHT_SHIFT);
	REGISTER_KEY("KEY_RIGHT_CONTROL", KeyboardKey::KEY_RIGHT_CONTROL);
	REGISTER_KEY("KEY_RIGHT_ALT", KeyboardKey::KEY_RIGHT_ALT);
	REGISTER_KEY("KEY_RIGHT_SUPER", KeyboardKey::KEY_RIGHT_SUPER);
	REGISTER_KEY("KEY_KB_MENU", KeyboardKey::KEY_KB_MENU);
	REGISTER_KEY("KEY_KP_0", KeyboardKey::KEY_KP_0);
	REGISTER_KEY("KEY_KP_1", KeyboardKey::KEY_KP_1);
	REGISTER_KEY("KEY_KP_2", KeyboardKey::KEY_KP_2);
	REGISTER_KEY("KEY_KP_3", KeyboardKey::KEY_KP_3);
	REGISTER_KEY("KEY_KP_4", KeyboardKey::KEY_KP_4);
	REGISTER_KEY("KEY_KP_5", KeyboardKey::KEY_KP_5);
	REGISTER_KEY("KEY_KP_6", KeyboardKey::KEY_KP_6);
	REGISTER_KEY("KEY_KP_7", KeyboardKey::KEY_KP_7);
	REGISTER_KEY("KEY_KP_8", KeyboardKey::KEY_KP_8);
	REGISTER_KEY("KEY_KP_9", KeyboardKey::KEY_KP_9);
	REGISTER_KEY("KEY_KP_DECIMAL", KeyboardKey::KEY_KP_DECIMAL);
	REGISTER_KEY("KEY_KP_DIVIDE", KeyboardKey::KEY_KP_DIVIDE);
	REGISTER_KEY("KEY_KP_MULTIPLY", KeyboardKey::KEY_KP_MULTIPLY);
	REGISTER_KEY("KEY_KP_SUBTRACT", KeyboardKey::KEY_KP_SUBTRACT);
	REGISTER_KEY("KEY_KP_ADD", KeyboardKey::KEY_KP_ADD);
	REGISTER_KEY("KEY_KP_ENTER", KeyboardKey::KEY_KP_ENTER);
	REGISTER_KEY("KEY_KP_EQUAL", KeyboardKey::KEY_KP_EQUAL);	
	//Android key buttons
	REGISTER_KEY("KEY_BACK", KeyboardKey::KEY_BACK);	
	REGISTER_KEY("KEY_MENU", KeyboardKey::KEY_MENU);	
	REGISTER_KEY("KEY_VOLUME_UP", KeyboardKey::KEY_VOLUME_UP);	
	REGISTER_KEY("KEY_VOLUME_DOWN", KeyboardKey::KEY_VOLUME_DOWN);	

	const char *MouseKeyStr = "MouseButton";
	asEngine->RegisterEnum(MouseKeyStr);
#define REGISTER_MOUSEBUTTON(x, y) asEngine->RegisterEnumValue(MouseKeyStr, x, y);

	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_LEFT", MouseButton::MOUSE_BUTTON_LEFT);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_RIGHT", MouseButton::MOUSE_BUTTON_RIGHT);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_MIDDLE", MouseButton::MOUSE_BUTTON_MIDDLE);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_SIDE", MouseButton::MOUSE_BUTTON_SIDE);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_EXTRA", MouseButton::MOUSE_BUTTON_EXTRA);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_FORWARD", MouseButton::MOUSE_BUTTON_FORWARD);
	REGISTER_MOUSEBUTTON("MOUSE_BUTTON_BACK", MouseButton::MOUSE_BUTTON_BACK);
}

//Something weird happens if we call the raylib one directly
inline void asClearBG(Color& color)
{
	ClearBackground(color);
}

inline void asDrawPixel(int posX, int posY, Color& color) { DrawPixel(posX, posY, color); }
inline void asDrawPixelV(Vector2& position, Color& color) { DrawPixelV(position, color); }

inline void asDrawLine(int startPosX, int startPosY, int endPosX, int endPosY, Color& color) { DrawLine(startPosX, startPosY, endPosX, endPosY, color); }
inline void asDrawLineV(Vector2& startPos, Vector2& endPos, Color& color) { DrawLineV(startPos, endPos, color); }
inline void asDrawLineEx(Vector2& startPos, Vector2& endPos, float thick, Color& color) { DrawLineEx(startPos, endPos, thick, color); }
inline void asDrawLineBezier(Vector2& startPos, Vector2& endPos, float thick, Color& color) {  DrawLineBezier(startPos, endPos, thick, color); }
inline void asDrawLineBezierQuad(Vector2& startPos, Vector2& endPos, Vector2& controlPos, float thick, Color& color) {  DrawLineBezierQuad(startPos, endPos, controlPos, thick, color); }
inline void asDrawLineBezierCubic(Vector2& startPos, Vector2& endPos, Vector2& startControlPos, Vector2& endControlPos, float thick, Color& color) {  DrawLineBezierCubic(startPos, endPos, startControlPos, endControlPos, thick, color); }

inline void asDrawCircle(int centerX, int centerY, float radius, Color& color) { DrawCircle(centerX, centerY, radius, color); }

inline void asDrawRectangle(int posX, int posY, int width, int height, Color& color) { DrawRectangle(posX, posY, width, height, color); }
inline void asDrawRectangleV(Vector2& pos, Vector2& size, Color& color) { DrawRectangleV(pos, size, color); }
inline void asDrawRectangleRec(Rectangle& rec, Color& color) { DrawRectangleRec(rec, color); }
inline void asDrawRectanglePro(Rectangle& rec, Vector2& origin, float rotation, Color& color) { DrawRectanglePro(rec, origin, rotation, color); }

inline void asDrawTriangle(Vector2& v1, Vector2& v2, Vector2& v3, Color& color) { DrawTriangle(v1, v2, v3, color); }

inline bool asCheckCollisionRecs(Rectangle& rec1, Rectangle& rec2) { return CheckCollisionRecs(rec1, rec2); }
inline bool asCheckCollisionPointRec(Vector2& point, Rectangle& rec) { return CheckCollisionPointRec(point, rec); }

inline void asDrawText(std::string& text, int posX, int posY, int fontSize, Color& color)
{
	DrawText(text.c_str(), posX, posY, fontSize, color);
}


inline int asGetScreenWidth() { return GetScreenWidth();}
inline int asGetScreenHeight() { return GetScreenHeight();}
inline void asSetWindowSize(int width, int height) { SetWindowSize(width, height); }

Vector2 mouse_pos;
inline Vector2& asGetMousePosition() { mouse_pos = GetMousePosition(); return mouse_pos;}

#define REGISTER_FUNC(x, y) asEngine->RegisterGlobalFunction(x, asFUNCTION(y), asCALL_STDCALL);

inline void RegisterRcore(asIScriptEngine *asEngine)
{
	int r;
	//Window-related functions
	r = REGISTER_FUNC("void CloseWindow()", CloseWindow);
	r = REGISTER_FUNC("int GetScreenWidth()", asGetScreenWidth);
	r = REGISTER_FUNC("int GetScreenHeight()", asGetScreenHeight);
	r = REGISTER_FUNC("void SetWindowSize(int, int)", asSetWindowSize);

	//Cursor-related functions
	r = REGISTER_FUNC("void ShowCursor()", ShowCursor);
	r = REGISTER_FUNC("void HideCursor()", HideCursor);
	r = REGISTER_FUNC("bool IsCursorHidden()", IsCursorHidden);
	r = REGISTER_FUNC("void EnableCursor()", EnableCursor);
	r = REGISTER_FUNC("void DisableCursor()", DisableCursor);
	r = REGISTER_FUNC("bool IsCursorOnScreen()", IsCursorOnScreen);

	// Input-related funcions: keyboard
	r = REGISTER_FUNC("bool IsKeyPressed(int)", IsKeyPressed);
	r = REGISTER_FUNC("bool IsKeyDown(int)", IsKeyDown);
	r = REGISTER_FUNC("bool IsKeyReleased(int)", IsKeyReleased);
	r = REGISTER_FUNC("bool IsKeyUp(int)", IsKeyUp);

	// Input-related functions: mouse
	r = REGISTER_FUNC("bool IsMouseButtonPressed(int)", IsMouseButtonPressed);
	r = REGISTER_FUNC("bool IsMouseButtonDown(int)", IsMouseButtonDown);
	r = REGISTER_FUNC("bool IsMouseButtonReleased(int)", IsMouseButtonReleased);
	r = REGISTER_FUNC("bool IsMouseButtonUp(int)", IsMouseButtonUp);
	r = REGISTER_FUNC("int GetMouseX()", GetMouseX);
	r = REGISTER_FUNC("int GetMouseY()", GetMouseY);
	r = REGISTER_FUNC("Vector2& GetMousePosition()", asGetMousePosition);

	//Drawing-related functions
	r = REGISTER_FUNC("void ClearBackground(Color& in)", asClearBG);
	r = REGISTER_FUNC("void BeginDrawing()", BeginDrawing);
 	r = REGISTER_FUNC("void EndDrawing()", EndDrawing);
}

inline void RegisterRshapes(asIScriptEngine* asEngine)
{
	int r;
	r = REGISTER_FUNC("void DrawPixel(int, int, Color& in)", asDrawPixel);
	r = REGISTER_FUNC("void DrawPixelV(Vector2& in, Color& in)", asDrawPixelV);

	r = REGISTER_FUNC("void DrawLine(int, int, int, int, Color& in)", asDrawLine);
	r = REGISTER_FUNC("void DrawLineV(Vector2& in, Vector2& in, Color& in)", asDrawLineV);
	r = REGISTER_FUNC("void DrawLineEx(Vector2& in, Vector2& in, float, Color& in)", asDrawLineEx);
	r = REGISTER_FUNC("void DrawLineBezier(Vector2& in, Vector2& in, float, Color& in)", asDrawLineBezier);
	r = REGISTER_FUNC("void DrawLineBezierQuad(Vector2& in, Vector2& in, Vector2& in, float, Color& in)", DrawLineBezierQuad);
	r = REGISTER_FUNC("void DrawLineBezierCubic(Vector2& in, Vector2& in, Vector2& in, Vector2& in, float, Color& in)", DrawLineBezierCubic);

	r = REGISTER_FUNC("void DrawCircle(int, int, float, Color& in)", asDrawCircle);
	r = REGISTER_FUNC("void DrawCircleSector(Vector2& in, float, float, float, int, Color& in)", DrawCircleSector);

	r = REGISTER_FUNC("void DrawRectangle(int, int, int, int, Color& in)", asDrawRectangle);
	r = REGISTER_FUNC("void DrawRectangleV(Vector2& in, Vector2& in, Color& in)", asDrawRectangleV);
	r = REGISTER_FUNC("void DrawRectangleRec(Rectangle& in, Color& in)", asDrawRectangleRec);
	r = REGISTER_FUNC("void DrawRectanglePro(Rectangle& in, Vector2& in, float, Color& in)", asDrawRectanglePro);

	r = REGISTER_FUNC("void DrawTriangle(Vector2& in, Vector2& in, Vector2& in, Color& in)", asDrawTriangle);

	r = REGISTER_FUNC("bool CheckCollisionRecs(Rectangle& in, Rectangle& in)", asCheckCollisionRecs);
	r = REGISTER_FUNC("bool CheckCollisionPointRec(Vector2& in, Rectangle& in)", asCheckCollisionPointRec);
}

inline Texture asLoadTexture(std::string& filename) { return LoadTexture(filename.c_str()); }
inline void asUnloadTexture(Texture& texture) { UnloadTexture(texture); }
inline void asDrawTexture(Texture& texture, int posX, int posY, Color& tint) { DrawTexture(texture, posX, posY, tint); }
inline void asDrawTextureV(Texture& texture, Vector2& position, Color& tint) { DrawTextureV(texture, position, tint); }
inline void asDrawTextureEx(Texture& texture, Vector2& position, float rotation, float scale, Color& tint) { DrawTextureEx(texture, position, rotation, scale, tint); }
inline void asDrawTextureRec(Texture& texture, Rectangle& source, Vector2& position, Color& tint) { DrawTextureRec(texture, source, position, tint); }
inline void asDrawTexturePro(Texture& texture, Rectangle& source, Rectangle& dest, Vector2& origin, float rotation, Color& tint) { DrawTexturePro(texture, source, dest, origin, rotation, tint); }

inline void RegisterRtextures(asIScriptEngine *asEngine)
{
	int r;
	r = REGISTER_FUNC("Texture LoadTexture(string& in)", asLoadTexture);
	r = REGISTER_FUNC("void UnloadTexture(Texture& in)", asUnloadTexture);

	// Texture drawing functions
	r = REGISTER_FUNC("void DrawTexture(Texture& in, int, int, Color& in)", asDrawTexture);
	r = REGISTER_FUNC("void DrawTextureV(Texture& in, Vector2& in, Color& in)", asDrawTextureV);
}

void RegisterRaylibBuiltins(asIScriptEngine* asEngine)
{
	int r;

	RegisterStructs(asEngine);
	RegisterEnums(asEngine);

	r = asEngine->RegisterGlobalProperty("Color LIGHTGRAY", &asLIGHTGRAY);
	r = asEngine->RegisterGlobalProperty("Color GRAY", &asGRAY);
	r = asEngine->RegisterGlobalProperty("Color DARKGRAY", &asDARKGRAY);
	r = asEngine->RegisterGlobalProperty("Color YELLOW", &asYELLOW);
	r = asEngine->RegisterGlobalProperty("Color GOLD", &asGOLD);
	r = asEngine->RegisterGlobalProperty("Color ORANGE", &asORANGE);
	r = asEngine->RegisterGlobalProperty("Color PINK", &asPINK);
	r = asEngine->RegisterGlobalProperty("Color RED", &asRED);
	r = asEngine->RegisterGlobalProperty("Color MAROON", &asMAROON);
	r = asEngine->RegisterGlobalProperty("Color GREEN", &asGREEN);
	r = asEngine->RegisterGlobalProperty("Color LIME", &asLIME);
	r = asEngine->RegisterGlobalProperty("Color DARKGREEN", &asDARKGREEN);
	r = asEngine->RegisterGlobalProperty("Color SKYBLUE", &asSKYBLUE);
	r = asEngine->RegisterGlobalProperty("Color BLUE", &asBLUE);
	r = asEngine->RegisterGlobalProperty("Color DARKBLUE", &asDARKBLUE);
	r = asEngine->RegisterGlobalProperty("Color PURPLE", &asPURPLE);
	r = asEngine->RegisterGlobalProperty("Color VIOLET", &asVIOLET);
	r = asEngine->RegisterGlobalProperty("Color DARKPURPLE", &asDARKPURPLE);
	r = asEngine->RegisterGlobalProperty("Color BEIGE", &asBEIGE);
	r = asEngine->RegisterGlobalProperty("Color BROWN", &asBROWN);
	r = asEngine->RegisterGlobalProperty("Color DARKBROWN", &asDARKBROWN);

	r = asEngine->RegisterGlobalProperty("Color WHITE", &asWHITE);
	r = asEngine->RegisterGlobalProperty("Color BLACK", &asBLACK);
	r = asEngine->RegisterGlobalProperty("Color BLANK", &asBLANK);
	r = asEngine->RegisterGlobalProperty("Color MAGENTA", &asMAGENTA);
	r = asEngine->RegisterGlobalProperty("Color RAYWHITE", &asRAYWHITE);

	RegisterRcore(asEngine);
	RegisterRshapes(asEngine);
	RegisterRtextures(asEngine);


 	r = REGISTER_FUNC("void DrawText(string& in, int, int, int, Color& in)", asDrawText);	
}
