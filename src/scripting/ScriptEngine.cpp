#include "builtins.h"
#include "raylib_builtins.h"

#include "angelscript.h"
#include "scriptstdstring/scriptstdstring.h"
#include "scriptarray/scriptarray.h"
#include "scriptdictionary/scriptdictionary.h"
#include "scriptmath/scriptmath.h"
#include "scriptfile/scriptfile.h"
#include "scriptbuilder/scriptbuilder.h"

#include <iostream>

asIScriptEngine* asEngine;
asIScriptContext* ctx;
asIScriptModule* mod;
asIScriptFunction* initFunc;
asIScriptFunction* tickFunc;
asIScriptFunction* drawFunc;

void MessageCallback(const asSMessageInfo* msg, void* param){
	const char* type = "ERR ";
	if(msg->type == asMSGTYPE_WARNING) type = "WARN";
	else if(msg->type == asMSGTYPE_INFORMATION) type = "INFO";

	std::cout << msg->section << "(" << msg->row << ", " << msg->col << ") : " << type << " : " << msg->message << std::endl;
}


void InitScriptingEngine()
{
	asEngine = asCreateScriptEngine();

	int r;
	asEngine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);
	
	RegisterStdString(asEngine);
	RegisterScriptArray(asEngine, true);
	RegisterScriptDictionary(asEngine);
	RegisterScriptMath(asEngine);
	RegisterScriptFile(asEngine);

	if (!strstr(asGetLibraryOptions(), "AS_MAX_PORTABILITY")) 
	{
		RegisterGlobals(asEngine);
		RegisterRaylibBuiltins(asEngine);
	}
}

int CompileScript()
{
	CScriptBuilder scriptBuilder;
	FILE* f = fopen("game.as", "rb");
	if(!f)
	{
		std::cout << "Failed to open gamecode!\n";
		return -1;
	}

	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);

	std::string script;
	script.resize(len);
	size_t c = fread(&script[0], len, 1, f);
	fclose(f);

	if(!c) return -2;

	int r = scriptBuilder.StartNewModule(asEngine, "script");

	if(r < 0) return -3;

	r = scriptBuilder.AddSectionFromFile("game.as");

	if(r < 0) return -4;

	r = scriptBuilder.BuildModule();

	if(r < 0) return -5;

	ctx = asEngine->CreateContext();

	if(!ctx)
	{
		asEngine->Release();
		return -6;
	}

	mod = asEngine->GetModule("script", asGM_ONLY_IF_EXISTS);
	if(!mod) return -7;

	return 0;
}

void RunScriptInit()
{
	initFunc = mod->GetFunctionByDecl("void init()");
	if(!initFunc) return;

	ctx->Prepare(initFunc);
	ctx->Execute();
	//ctx->Unprepare();

	tickFunc = mod->GetFunctionByDecl("void tick()");
	drawFunc = mod->GetFunctionByDecl("void draw()");
}

void RunScriptTick()
{
	if(!tickFunc) return;
	static int r;

	ctx->Prepare(tickFunc);
	r = ctx->Execute();
}

void RunScriptDraw()
{
	if(!drawFunc) return;
	static int r;	

	ctx->Prepare(drawFunc);
	r = ctx->Execute();
}