#ifndef CH_SCRIPTING_ENGINE
#define CH_SCRIPTING_ENGINE

#include "angelscript.h"

extern asIScriptEngine* asEngine;
void InitScriptingEngine();
int CompileScript();
void RunScriptTick();
void RunScriptDraw();
int RunScriptInit();

#endif