#ifndef CH_RL_BUILTINS
#define CH_RL_BUILTINS
#include "angelscript.h"

void RegisterRaylibBuiltins(asIScriptEngine* asEngine);

#endif